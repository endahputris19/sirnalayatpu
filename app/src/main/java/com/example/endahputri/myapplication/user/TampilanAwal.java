package com.example.endahputri.myapplication.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

import com.example.endahputri.myapplication.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.example.endahputri.myapplication.admin.FormLoginAdmin;


/**
 * Created by Endah Putri on 7/3/2019.
 */

public class TampilanAwal extends AppCompatActivity {
    @BindView(R.id.btn_daftar_awal)
    Button daftarAwal;
    @BindView(R.id.btn_admin)
    TextView masukAdmin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tampilan_awal);
        ButterKnife.bind(this);

    }
    @OnClick(R.id.btn_daftar_awal)
    public void onGetUser() {
            Intent intent = new Intent(TampilanAwal.this, FormPetaBlok.class);
            startActivity(intent);
        }

    @OnClick(R.id.btn_admin)
    public void onGetAdmin() {
        Intent intent = new Intent(TampilanAwal.this, FormLoginAdmin.class);
        startActivity(intent);
    }
}



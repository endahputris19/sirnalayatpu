package com.example.endahputri.myapplication.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.endahputri.myapplication.R;
import com.example.endahputri.myapplication.dataTemp.DataTemp;
import com.example.endahputri.myapplication.model.DataModel;

import java.util.ArrayList;

/**
 * Created by Endah Putri on 7/2/2019.
 */

public class DataUserViewAdapter extends RecyclerView.Adapter<DataUserViewAdapter.ViewHolder> {
    private DataListener listener;
    private ArrayList<DataModel> datas;
    private Context context;

    public DataUserViewAdapter(Context context) {
        this.datas = new ArrayList<>();
        this.context = context;
    }

    public void setListener(DataListener listener) {
        this.listener = listener;
    }

    public void setDatas(ArrayList<DataModel> datas) {
        this.datas = datas;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        //todo utuk pengenalan Texview dll
        TextView namaUser;
        TextView ttlUser;
        TextView tglWafatUser;
        TextView lokasiKuburanUser;

        public ViewHolder(View itemView) {
            super(itemView);
            //todo pengganti onCreate
            namaUser = (TextView) itemView.findViewById(R.id.nama_user);
            ttlUser = (TextView) itemView.findViewById(R.id.tanggal_lahir);
            tglWafatUser = (TextView) itemView.findViewById(R.id.tanggal_wafat);
            lokasiKuburanUser = (TextView) itemView.findViewById(R.id.peta_lokasi);
        }
    }

    @Override
    public DataUserViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //todo set layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_data_user_view, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DataUserViewAdapter.ViewHolder holder, int position) {
        final DataModel dataModel = datas.get(position);
        DataTemp.JUMLAHUSER = String.valueOf(datas.size());
        final String namaUser = dataModel.getNamaUser();
        final String ttlUser = dataModel.getTtlUser();
        final String tglWafatUser = dataModel.getTanggalWafat();
        holder.namaUser.setText(namaUser);
        holder.ttlUser.setText(ttlUser);
        holder.tglWafatUser.setText(tglWafatUser);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onSelectedItem(dataModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public interface DataListener {
        void onSelectedItem(DataModel data);
    }
}

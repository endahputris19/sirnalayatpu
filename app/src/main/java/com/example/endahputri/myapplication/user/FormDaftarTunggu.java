package com.example.endahputri.myapplication.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.endahputri.myapplication.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Endah Putri on 7/6/2019.
 */

public class FormDaftarTunggu extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_daftar_tunggu);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_home)
    public void onGetUser() {
            Intent intent = new Intent(FormDaftarTunggu.this, TampilanAwal.class);
            startActivity(intent);
        }
    }
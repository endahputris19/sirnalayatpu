package com.example.endahputri.myapplication.admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import com.example.endahputri.myapplication.R;
import com.example.endahputri.myapplication.user.FormDataUserAct;
import com.example.endahputri.myapplication.user.FormListAntrianMakam;
import com.example.endahputri.myapplication.user.FormPengajuAct;
import com.example.endahputri.myapplication.user.TampilanAwal;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import static com.example.endahputri.myapplication.dataTemp.DataTemp.PASSWORD;
import static com.example.endahputri.myapplication.dataTemp.DataTemp.USERDATA;
import static com.example.endahputri.myapplication.dataTemp.DataTemp.DATAPENGAJU;
import static com.example.endahputri.myapplication.network.ConfigURL.AKSES;

/**
 * Created by Endah Putri on 7/3/2019.
 */

public class FormLoginAdmin extends AppCompatActivity {
    @BindView(R.id.input_user)
    EditText inputUser;
    @BindView(R.id.input_pass)
    EditText inputPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_admin);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.btn_login_admin)
    public void onGetUser() {
        AKSES = "1";
        String inUser = inputUser.getText().toString();
        String inPass = inputPass.getText().toString();
        if (inUser.isEmpty()) {
            inputUser.setError("username kosong");
        }
        if (inPass.isEmpty()) {
            inputPass.setError("password kosong");
        }
        if (inUser.equals(USERDATA) && inPass.equals(PASSWORD)) {
            Intent intent = new Intent(FormLoginAdmin.this, FormListAntrianMakam.class);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Username atau Password salah", Toast.LENGTH_SHORT).show();
        }

    }

    @OnClick(R.id.btn_masuk_user)
    public void onGetPengaju() {
        Intent intent = new Intent(FormLoginAdmin.this, TampilanAwal.class);
        startActivity(intent);
    }
}

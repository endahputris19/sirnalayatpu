package com.example.endahputri.myapplication.admin;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import com.example.endahputri.myapplication.R;
import com.example.endahputri.myapplication.model.DataModel;
import com.example.endahputri.myapplication.model.DataTumpuk;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.endahputri.myapplication.dataTemp.DataTemp.DATAPENGAJU;
import static com.example.endahputri.myapplication.dataTemp.DataTemp.PETALOKASI;
import static com.example.endahputri.myapplication.network.ConfigURL.AKSES;
import static com.example.endahputri.myapplication.network.ConfigURL.ANTRIAN;
import static com.example.endahputri.myapplication.network.ConfigURL.TUMPUKAN;

/**
 * Created by Endah Putri on 7/13/2019.
 */

public class StatusPenumpukan extends AppCompatActivity {

    @BindView(R.id.switch_buttonA)
    Switch switchA;
    @BindView(R.id.switch_buttonB)
    Switch switchB;
    @BindView(R.id.switch_buttonC)
    Switch switchC;
    @BindView(R.id.switch_buttonD)
    Switch switchD;
    @BindView(R.id.switch_buttonE)
    Switch switchE;
    @BindView(R.id.switch_buttonF)
    Switch switchF;
    @BindView(R.id.btn_set_status)
    Button btnSetStatus;

    private DatabaseReference database;
    private FirebaseAnalytics mFirebaseAnalytics;
    String setBlokA, setBlokB, setBlokC, setBlokD, setBlokE, setBlokF;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.status_penumpukan);
        ButterKnife.bind(this);
        //todo config firebaseDatabase
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        database = FirebaseDatabase.getInstance().getReference();

        setListener();
    }

    void setListener() {
        switchA.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    setBlokA = "aktif";
                }
                else {
                    setBlokA = "nonaktif";
                }
            }
        });

        switchB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    setBlokB = "aktif";
                }
                else {
                    setBlokB = "nonaktif";
                }
            }
        });
        switchC.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    setBlokC = "aktif";
                }
                else {
                    setBlokC = "nonaktif";
                }
            }
        });
        switchD.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    setBlokD = "aktif";
                }
                else {
                    setBlokD = "nonaktif";
                }
            }
        });
        switchE.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    setBlokE = "aktif";
                }
                else {
                    setBlokE = "nonaktif";
                }
            }
        });
        switchF.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    setBlokF = "aktif";
                }
                else {
                    setBlokF = "nonaktif";
                }
            }
        });

    }

        //todo ini buat submit ke firebase
    public void setData(DataTumpuk datas) {
        AKSES = "0";
        database.child(TUMPUKAN)
                .setValue(datas);
    }

    @OnClick(R.id.btn_set_status)
    public void onSetStatus(){
        Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();
        setData(new DataTumpuk(setBlokA,setBlokB,setBlokC,setBlokD,setBlokE,setBlokF));
    }


}


package com.example.endahputri.myapplication.network;

import com.example.endahputri.myapplication.model.DataModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Endah Putri on 7/2/2019.
 */

public interface Routes {
    @GET("/{fild}.json")
    Call<DataModel> getDataUser(@Path("fild") String fild);
}

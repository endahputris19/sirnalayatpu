package com.example.endahputri.myapplication.user;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.endahputri.myapplication.R;
import com.example.endahputri.myapplication.adapter.DataUserAdapter;
import com.example.endahputri.myapplication.admin.StatusPenumpukan;
import com.example.endahputri.myapplication.model.DataModel;
import com.example.endahputri.myapplication.network.NetworkClient;
import com.example.endahputri.myapplication.network.Routes;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.endahputri.myapplication.dataTemp.DataTemp.DATAPENGAJU;
import static com.example.endahputri.myapplication.dataTemp.DataTemp.JUMLAHUSER;
import static com.example.endahputri.myapplication.dataTemp.DataTemp.PETALOKASI;
import static com.example.endahputri.myapplication.network.ConfigURL.AKSES;
import static com.example.endahputri.myapplication.network.ConfigURL.ANTRIAN;
import static com.example.endahputri.myapplication.network.ConfigURL.DATAUSER;

/**
 * Created by Endah Putri on 7/2/2019.
 */

public class FormListAntrianMakam extends AppCompatActivity {
    @BindView(R.id.list_makam)
    RecyclerView recyclerView;
    @BindView(R.id.status)
    TextView status;
    @BindView(R.id.btn_selesai)
    Button btnSelesai;
    @BindView(R.id.btn_status_penumpukan)
    Button btnStatPenumpukan;

    DataUserAdapter dataUserAdapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<DataModel> dataListMakam;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_list_antrian_view);
        ButterKnife.bind(this);

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        databaseReference = FirebaseDatabase.getInstance().getReference();

        if (AKSES.equals("0")) {
            btnStatPenumpukan.setVisibility(View.GONE);
        }
        getData(PETALOKASI);
    }


    public void getData(final String list) {
        Routes client = NetworkClient.getClient().create(Routes.class);
        Call<DataModel> call = client.getDataUser(list);
        call.enqueue(new Callback<DataModel>() {
            @Override
            public void onResponse(Call<DataModel> call, Response<DataModel> response) {
                if (response.isSuccessful()) {
                    Log.d("ok respon", "mantul");
                    databaseReference.child(ANTRIAN).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            dataListMakam = new ArrayList<>();
                            for (DataSnapshot mDataSnap : dataSnapshot.getChildren()) {
                                DataModel dataModel = mDataSnap.getValue(DataModel.class);
                                dataListMakam.add(dataModel);
                                JUMLAHUSER = String.valueOf(dataListMakam.size());
                            }
                            Log.d("isi", "isinya" + JUMLAHUSER);
                            if (JUMLAHUSER.isEmpty() || JUMLAHUSER.equals("0") || JUMLAHUSER.equals(0)) {
                                status.setText("List Kosong");
                            }
                            {
                                status.setText("List Antrian Konfirmasi: " + JUMLAHUSER);
                            }
                            dataUserAdapter = new DataUserAdapter(FormListAntrianMakam.this);
                            //BEDAIN AKSES ANTARA USER DAN ADMIN
                            dataUserAdapter.setListener(new DataUserAdapter.DataListener() {
                                @Override
                                public void onSelectedItem(final DataModel data) {
                                    if (AKSES.equals("0")) {
                                        btnStatPenumpukan.setVisibility(View.GONE);
                                        Toast.makeText(FormListAntrianMakam.this, "Permintan Kamu Masih Di proses", Toast.LENGTH_SHORT).show();
                                    }
                                    if (AKSES.equals("1")) {
                                        btnStatPenumpukan.setVisibility(View.VISIBLE);
                                        final String namaUser = data.getNamaUser();
                                        final String tglUser = data.getTtlUser();
                                        final String wafatUser = data.getTanggalWafat();
                                        final String agamaUser = data.getAgama();
                                        final String hubUser = data.getHubungan();
                                        final String warisUser = data.getNamaahliwaris();
                                        final String alamatUser = data.getAlamat();
                                        PETALOKASI = data.getLokasi();
                                        DATAPENGAJU = data.getPengaju();
                                        View view;
                                        view = getLayoutInflater().inflate(R.layout.modal_konfirmasi, null);
                                        Button konfirmasi = (Button) view.findViewById(R.id.btn_konfirmasi);
                                        final BottomSheetDialog dialog = new BottomSheetDialog(FormListAntrianMakam.this);
                                        dialog.setContentView(view);
                                        dialog.show();
                                        konfirmasi.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                                setData(new DataModel(
                                                        namaUser,
                                                        tglUser,
                                                        wafatUser,
                                                        agamaUser,
                                                        hubUser,
                                                        warisUser,
                                                        alamatUser,
                                                        PETALOKASI,
                                                        DATAPENGAJU));
                                                Toast.makeText(FormListAntrianMakam.this, "Push Data", Toast.LENGTH_SHORT).show();
                                                dialog.dismiss();
                                            }
                                        });
                                    }

                                }
                            });
                            recyclerView.setAdapter(dataUserAdapter);
                            dataUserAdapter.setDatas(dataListMakam);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<DataModel> call, Throwable t) {
                Toast.makeText(FormListAntrianMakam.this, "error" + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @OnClick(R.id.btn_selesai)
    public void onGetUser() {
        Intent intent = new Intent(FormListAntrianMakam.this, TampilanAwal.class);
        startActivity(intent);
        finishAffinity();
    }

    @OnClick(R.id.btn_status_penumpukan)
    public void onGetStat() {
        Intent intent = new Intent(FormListAntrianMakam.this, StatusPenumpukan.class);
        startActivity(intent);
        finishAffinity();
    }

    //todo ini buat submit ke firebase
    public void setData(DataModel datas) {
        databaseReference.child(DATAUSER)
                .child(PETALOKASI)
                .child(DATAPENGAJU.replace(' ', '_'))
                .setValue(datas);

        databaseReference.child(ANTRIAN).child(DATAPENGAJU).removeValue();
    }
}

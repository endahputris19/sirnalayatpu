package com.example.endahputri.myapplication.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.endahputri.myapplication.R;

/**
 * Created by Endah Putri on 7/9/2019.
 */

public class SliderAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;

    public SliderAdapter(Context context){
        this.context = context;
    }
    //array
    public int[] slide_image = {
            R.drawable.eat_icon,
            R.drawable.sleep_icon,
            R.drawable.code_icon
    };
    public String[] slide_headings = {
            "PANDUAN ",
            "WAKTU",
            "BIAYA"
    };

    public String[] slide_descs = {
            "Untuk melakukan pendaftaran pemakaman dapat dilakukan seperti dibawah ini :\n" +
                    "\n" +
                    "1. Pilih Tombol DAFTAR PEMAKAMAN\n" +
                    "2. Pilih Blok Pemakaman Yang Tersedia\n" +
                    "3. Pilih Tombol Pengajuan\n" +
                    "4. Masukkan Nama Pengaju\n" +
                    "5. Isi Formulir Pendaftaran Pemakaman\n" +
                    "6. Pilih Tombol Daftar Untuk Melakukan Proses Pendaftaran\n" +
                    "\n" +
                    "*Pendaftaran Pemakaman Akan Terdaftar Jika Sudah Dikonfirmasi Oleh Admin\n" +
                    "*Untuk Melihat Status Pendaftaran Silahkan Buka Menu Daftar Tunggu\n",


            "Untuk Melakukan Proses Pendaftaran Pemakaman, Maka Pengaju Harus Melakukan Proses Pendaftaran Maksimal 30 Menit Sebelum Dilaksanakannya Pemakaman ",


            "Berdasarkan Peraturan Daerah Kabupaten Cianjur No. 07 Tahun 2012\n " +
                    "Tentang Retribusi Pelayanan Persampahan/Kebersihan, Pelayanan Penyediaan dan/atau Penyeditan Kakus, dan Restribusi Pemakaman dan Pengabuan Mayat.\n" +
                    "\n" +
                    "-Biaya Administrasi Pemakaman : Rp.80.000,-\n" +
                    "-Pajak per 2 Tahun (heregistrasi) : Rp.40.000,-\n" +
                    "-Paket Jasa Ambulance : Rp.130.000,-\n"+
                    "(sudah termasuk dengan biaya penguburan)"
    };


    @Override
    public int getCount() {
        return slide_headings.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return view == (RelativeLayout) o;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slide_layout, container, false);

        ImageView slideImageView = (ImageView) view.findViewById(R.id.slideImage);
        TextView slideHeading= (TextView) view.findViewById(R.id.slideHeading);
        TextView slideDescription = (TextView) view.findViewById(R.id.slide_desc);

        slideImageView.setImageResource(slide_image[position]);
        slideHeading.setText(slide_headings[position]);
        slideDescription.setText(slide_descs[position]);

        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((RelativeLayout)object);
    }

}

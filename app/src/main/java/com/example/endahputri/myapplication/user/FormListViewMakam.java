package com.example.endahputri.myapplication.user;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.endahputri.myapplication.R;
import com.example.endahputri.myapplication.adapter.AutoFitGridLayoutManager;
import com.example.endahputri.myapplication.adapter.DataUserAdapter;
import com.example.endahputri.myapplication.adapter.DataUserViewAdapter;
import com.example.endahputri.myapplication.model.DataModel;
import com.example.endahputri.myapplication.network.NetworkClient;
import com.example.endahputri.myapplication.network.Routes;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.endahputri.myapplication.dataTemp.DataTemp.JUMLAHUSER;
import static com.example.endahputri.myapplication.dataTemp.DataTemp.PETALOKASI;
import static com.example.endahputri.myapplication.network.ConfigURL.DATAUSER;

/**
 * Created by Endah Putri on 7/2/2019.
 */

public class FormListViewMakam extends AppCompatActivity {
    @BindView(R.id.list_makam)
    RecyclerView recyclerView;
    @BindView(R.id.nama_blok)
    TextView namaBlok;
    @BindView(R.id.blok_terisi)
    TextView blokterisi;
    @BindView(R.id.btn_pengajuan)
    Button btnPengajuan;

    DataUserViewAdapter dataUserAdapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<DataModel> dataListMakam;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_list_view_makam);
        ButterKnife.bind(this);

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        databaseReference = FirebaseDatabase.getInstance().getReference();

        namaBlok.setText("Peta " + PETALOKASI.replace('_', ' '));
        JUMLAHUSER = "0";
        getData(PETALOKASI);
    }

    public void getData(final String list) {
        Routes client = NetworkClient.getClient().create(Routes.class);
        Call<DataModel> call = client.getDataUser(list);
        call.enqueue(new Callback<DataModel>() {
            @Override
            public void onResponse(Call<DataModel> call, Response<DataModel> response) {
                if (response.isSuccessful()) {
                    databaseReference.child(DATAUSER).child(PETALOKASI).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            dataListMakam = new ArrayList<>();
                            for (DataSnapshot mDataSnap : dataSnapshot.getChildren()) {
                                DataModel dataModel = mDataSnap.getValue(DataModel.class);
                                dataListMakam.add(dataModel);
                                JUMLAHUSER = String.valueOf(dataListMakam.size());
                            }
                            blokterisi.setText("Blok Telah Terisi: " + JUMLAHUSER + "/5");
                            SharedPreferences sharedPreferences = getSharedPreferences("DATA", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString(PETALOKASI, JUMLAHUSER);
                            editor.commit();
                            Log.d("isi", "isinya" + JUMLAHUSER);
                            if (JUMLAHUSER.isEmpty()){
                                JUMLAHUSER = "0";
                            }
                            int jumlah = Integer.parseInt(JUMLAHUSER);
                            if (jumlah >= 5 || JUMLAHUSER.isEmpty()) {
                                btnPengajuan.setEnabled(false);
                                btnPengajuan.setText("Pemakaman Sudah Penuh");
                                btnPengajuan.setBackgroundResource(R.drawable.background_button_nonactiv);
                            }
                            dataUserAdapter = new DataUserViewAdapter(FormListViewMakam.this);
                            dataUserAdapter.setListener(new DataUserViewAdapter.DataListener() {
                                @Override
                                public void onSelectedItem(DataModel data) {
                                    Toast.makeText(FormListViewMakam.this, "Makam dari " + data.getNamaUser(), Toast.LENGTH_SHORT).show();

                                }
                            });
                            recyclerView.setAdapter(dataUserAdapter);
                            recyclerView.setLayoutManager(new GridLayoutManager(FormListViewMakam.this, 2));
                            dataUserAdapter.setDatas(dataListMakam);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<DataModel> call, Throwable t) {
                Toast.makeText(FormListViewMakam.this, "error" + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @OnClick(R.id.btn_pengajuan)
    public void onGetUser() {
        Intent intent = new Intent(FormListViewMakam.this, FormPengajuAct.class);
        startActivity(intent);
    }
}

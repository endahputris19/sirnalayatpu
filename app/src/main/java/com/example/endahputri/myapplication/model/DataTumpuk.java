package com.example.endahputri.myapplication.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Endah Putri on 7/14/2019.
 */

public class DataTumpuk {
    @SerializedName("Blok_A")
    String blokA;
    @SerializedName("Blok_B")
    String blokB;
    @SerializedName("Blok_C")
    String blokC;
    @SerializedName("Blok_D")
    String blokD;
    @SerializedName("Blok_E")
    String blokE;
    @SerializedName("Blok_F")
    String blokF;

    public DataTumpuk(String blokA, String blokB, String blokC, String blokD, String blokE, String blokF) {
        this.blokA = blokA;
        this.blokB = blokB;
        this.blokC = blokC;
        this.blokD = blokD;
        this.blokE = blokE;
        this.blokF = blokF;
    }

    public DataTumpuk(){

    }

    public String getBlokA() {
        return blokA;
    }

    public void setBlokA(String blokA) {
        this.blokA = blokA;
    }

    public String getBlokB() {
        return blokB;
    }

    public void setBlokB(String blokB) {
        this.blokB = blokB;
    }

    public String getBlokC() {
        return blokC;
    }

    public void setBlokC(String blokC) {
        this.blokC = blokC;
    }

    public String getBlokD() {
        return blokD;
    }

    public void setBlokD(String blokD) {
        this.blokD = blokD;
    }

    public String getBlokE() {
        return blokE;
    }

    public void setBlokE(String blokE) {
        this.blokE = blokE;
    }

    public String getBlokF() {
        return blokF;
    }

    public void setBlokF(String blokF) {
        this.blokF = blokF;
    }
}

package com.example.endahputri.myapplication.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import com.example.endahputri.myapplication.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.endahputri.myapplication.dataTemp.DataTemp.DATAPENGAJU;

/**
 * Created by Endah Putri on 7/1/2019.
 */

public class FormPengajuAct extends AppCompatActivity {
    @BindView(R.id.isi_pengaju)
    EditText isiPengaju;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pengajuan);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_daftar_pengaju)
    public void onGetUser() {
        DATAPENGAJU = isiPengaju.getText().toString();
        if (DATAPENGAJU.isEmpty()) {
            isiPengaju.setError("Harap Di Isi!");
        } else {
            Intent intent = new Intent(FormPengajuAct.this, FormDataUserAct.class);
            startActivity(intent);
        }
    }

}

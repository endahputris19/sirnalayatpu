package com.example.endahputri.myapplication.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Endah Putri on 7/2/2019.
 */

public class DataModel {
    @SerializedName("namaUser")
    String namaUser;
    @SerializedName("ttlUser")
    String ttlUser;
    @SerializedName("tanggalWafat")
    String tanggalWafat;
    @SerializedName("agama")
    String agama;
    @SerializedName("hubungan")
    String hubungan;
    @SerializedName("namaahliwaris")
    String namaahliwaris;
    @SerializedName("alamat")
    String alamat;
    @SerializedName("lokasi")
    String lokasi;
    @SerializedName("namaPengaju")
    String pengaju;

    public DataModel(){

    }

    public DataModel(String namaUser, String ttlUser, String tanggalWafat, String agama, String hubungan, String namaahliwaris, String alamat, String lokasi, String pengaju) {
        this.namaUser = namaUser;
        this.ttlUser = ttlUser;
        this.tanggalWafat = tanggalWafat;
        this.agama = agama;
        this.hubungan = hubungan;
        this.namaahliwaris = namaahliwaris;
        this.alamat = alamat;
        this.lokasi = lokasi;
        this.pengaju = pengaju;
    }

    public String getPengaju() {
        return pengaju;
    }

    public void setPengaju(String pengaju) {
        this.pengaju = pengaju;
    }

    public String getNamaUser() {
        return namaUser;
    }

    public void setNamaUser(String namaUser) {
        this.namaUser = namaUser;
    }

    public String getTtlUser() {
        return ttlUser;
    }

    public void setTtlUser(String ttlUser) {
        this.ttlUser = ttlUser;
    }

    public String getTanggalWafat() {
        return tanggalWafat;
    }

    public void setTanggalWafat(String tanggalWafat) {
        this.tanggalWafat = tanggalWafat;
    }

    public String getAgama() {
        return agama;
    }

    public void setAgama(String agama) {
        this.agama = agama;
    }

    public String getHubungan() {
        return hubungan;
    }

    public void setHubungan(String hubungan) {
        this.hubungan = hubungan;
    }

    public String getNamaahliwaris() {
        return namaahliwaris;
    }

    public void setNamaahliwaris(String namaahliwaris) {
        this.namaahliwaris = namaahliwaris;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        alamat = alamat;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }
}

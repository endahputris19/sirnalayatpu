package com.example.endahputri.myapplication.network;

/**
 * Created by Endah Putri on 7/2/2019.
 */

public class ConfigURL {
    public static final String BASE_URL = "https://tugasakhirspt.firebaseio.com";
    public static final String DATAUSER = "user";
    public static final String KONFIRMASI = "konfirmasi";
    public static final String ANTRIAN = "antrian";
    public static final String TUMPUKAN = "tumpukan";
    public static String AKSES = "0";
}

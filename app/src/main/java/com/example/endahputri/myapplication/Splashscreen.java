package com.example.endahputri.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.example.endahputri.myapplication.user.ScreenSlide;
import com.example.endahputri.myapplication.user.TampilanAwal;

/**
 * Created by Endah Putri on 7/1/2019.
 */

public class Splashscreen extends AppCompatActivity{

    private static int splashInterval = 3000;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splashscreen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(Splashscreen.this, ScreenSlide.class);
                startActivity(i);
                this.finish();
            }
            private void finish(){

            }
        }, splashInterval);
    }
}
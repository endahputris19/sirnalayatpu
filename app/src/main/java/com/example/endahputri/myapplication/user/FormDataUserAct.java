package com.example.endahputri.myapplication.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import com.example.endahputri.myapplication.R;
import com.example.endahputri.myapplication.model.DataModel;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.endahputri.myapplication.dataTemp.DataTemp.DATAPENGAJU;
import static com.example.endahputri.myapplication.dataTemp.DataTemp.PETALOKASI;
import static com.example.endahputri.myapplication.network.ConfigURL.AKSES;
import static com.example.endahputri.myapplication.network.ConfigURL.ANTRIAN;

/**
 * Created by Endah Putri on 7/1/2019.
 */

public class FormDataUserAct extends AppCompatActivity {
    @BindView(R.id.namameninggal)
    EditText namaMeninggal;
    @BindView(R.id.tanggallahir)
    EditText tanggalLahir;
    @BindView(R.id.tanggalwafat)
    EditText tanggalWafat;
    @BindView(R.id.agama)
    EditText agama;
    @BindView(R.id.hubungan)
    EditText hubungan;
    @BindView(R.id.namaahliwaris)
    EditText namaAhliwaris;
    @BindView(R.id.alamat)
    EditText alamat;
    @BindView(R.id.lokasi)
    EditText lokasi;

    private DatabaseReference database;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_pendaftaran);
        ButterKnife.bind(this);
        //todo config firebaseDatabase
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        database = FirebaseDatabase.getInstance().getReference();
        lokasi.setText(PETALOKASI.replace(' ','_'));
        lokasi.setEnabled(false);
    }

    //todo fungsi Button dan falidasi form_pendaftaran kosong atau tidak
    @OnClick(R.id.btn_proses)
    public void onProses() {
        String namaUser = namaMeninggal.getText().toString();
        String tglUser = tanggalLahir.getText().toString();
        String wafatUser = tanggalWafat.getText().toString();
        String agamaUser = agama.getText().toString();
        String hubUser = hubungan.getText().toString();
        String warisUser = namaAhliwaris.getText().toString();
        String alamatUser = alamat.getText().toString();
        String lokasiUser = lokasi.getText().toString();

        if (namaUser.isEmpty()) {
            namaMeninggal.setError("Nama Harus Diisi");
        }
        if (tglUser.isEmpty()) {
            tanggalLahir.setError("Tanggal Lahir Harus Diisi");
        }
        if (wafatUser.isEmpty()) {
            tanggalWafat.setError("Tanggal Wafat Harus Diisi");
        }
        if (agamaUser.isEmpty()) {
            agama.setError("Agama Harus Diisi");
        }
        if (hubUser.isEmpty()) {
            hubungan.setError("Hubungan Harus Diisi");
        }
        if (warisUser.isEmpty()) {
            namaAhliwaris.setError("Nama Ahli Waris Harus Diisi");
        }
        if (alamatUser.isEmpty()) {
            alamat.setError("Alamat Harus Diisi");
        } else {
            setData(new DataModel(
                    namaUser, tglUser, wafatUser, agamaUser, hubUser, warisUser, alamatUser, lokasiUser, DATAPENGAJU.replace(' ', '_')
            ));
            Toast.makeText(this, "Push Data", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(FormDataUserAct.this, FormListAntrianMakam.class);
            startActivity(intent);
        }
    }

    //todo ini buat submit ke firebase
    public void setData(DataModel datas) {
        AKSES = "0";
        database.child(ANTRIAN)
//                .child(EETALOKASI)
                .child(DATAPENGAJU.replace(' ', '_'))
                .setValue(datas);
    }
}

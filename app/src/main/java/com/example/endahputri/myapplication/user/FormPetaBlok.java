package com.example.endahputri.myapplication.user;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.endahputri.myapplication.R;
import com.example.endahputri.myapplication.dataTemp.DataTemp;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.endahputri.myapplication.dataTemp.DataTemp.PETALOKASI;
import static com.example.endahputri.myapplication.network.ConfigURL.AKSES;

/**
 * Created by Endah Putri on 7/2/2019.
 */

public class FormPetaBlok extends AppCompatActivity {
    @BindView(R.id.btn_bloka)
    Button blokA;
    @BindView(R.id.btn_blokb)
    Button blokB;
    @BindView(R.id.btn_blokc)
    Button blokC;
    @BindView(R.id.btn_blokd)
    Button blokD;
    @BindView(R.id.btn_bloke)
    Button blokE;
    @BindView(R.id.btn_blokf)
    Button blokF;
    @BindView(R.id.notif_isi_a)
    TextView notifIsiA;
    @BindView(R.id.notif_isi_b)
    TextView notifIsiB;
    @BindView(R.id.notif_isi_c)
    TextView notifIsiC;
    @BindView(R.id.notif_isi_d)
    TextView notifIsiD;
    @BindView(R.id.notif_isi_e)
    TextView notifIsiE;
    @BindView(R.id.notif_isi_f)
    TextView notifIsiF;
    @BindView(R.id.btn_daftar_tunggu)
    Button btnDaftarTunggu;

    String isiBlokA, isiBlokB, isiBlokC, isiBlokD, isiBlokE, isiBlokF;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.from_peta_blok);
        ButterKnife.bind(this);
        SharedPreferences sharedPreferences = getSharedPreferences("DATA", MODE_PRIVATE);
        isiBlokA = sharedPreferences.getString("blok_a", "");
        isiBlokB = sharedPreferences.getString("blok_b", "");
        isiBlokC = sharedPreferences.getString("blok_c", "");
        isiBlokD = sharedPreferences.getString("blok_d", "");
        isiBlokE = sharedPreferences.getString("blok_e", "");
        isiBlokF = sharedPreferences.getString("blok_f", "");
        notifIsiA.setText(isiBlokA);
        notifIsiB.setText(isiBlokB);
        notifIsiC.setText(isiBlokC);
        notifIsiD.setText(isiBlokD);
        notifIsiE.setText(isiBlokE);
        notifIsiF.setText(isiBlokF);
    }

    @OnClick(R.id.btn_bloka)
    public void onProsesA() {
        Toast.makeText(this, "Blok A", Toast.LENGTH_SHORT).show();
        PETALOKASI = "blok_a";
        Intent intent = new Intent(FormPetaBlok.this, FormListViewMakam.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_blokb)
    public void onProsesB() {
        Toast.makeText(this, "Blok B", Toast.LENGTH_SHORT).show();
        PETALOKASI = "blok_b";
        Intent intent = new Intent(FormPetaBlok.this, FormListViewMakam.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_blokc)
    public void onProsesC() {
        Toast.makeText(this, "Blok C", Toast.LENGTH_SHORT).show();
        PETALOKASI = "blok_c";
        Intent intent = new Intent(FormPetaBlok.this, FormListViewMakam.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_blokd)
    public void onProsesD() {
        Toast.makeText(this, "Blok D", Toast.LENGTH_SHORT).show();
        PETALOKASI = "blok_d";
        Intent intent = new Intent(FormPetaBlok.this, FormListViewMakam.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_bloke)
    public void onProsesE() {
        Toast.makeText(this, "Blok E", Toast.LENGTH_SHORT).show();
        PETALOKASI = "blok_e";
        Intent intent = new Intent(FormPetaBlok.this, FormListViewMakam.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_blokf)
    public void onProsesF() {
        Toast.makeText(this, "Blok F", Toast.LENGTH_SHORT).show();
        PETALOKASI = "blok_f";
        Intent intent = new Intent(FormPetaBlok.this, FormListViewMakam.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_daftar_tunggu)
    public void onBtnDaftarTunggu() {
        AKSES = "0";
        Intent intent = new Intent(FormPetaBlok.this, FormListAntrianMakam.class);
        startActivity(intent);
    }

}



